﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace DAEAvz_lab11.Models
{
    public class Student : Person
    {
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "EnrollmentDate")]
        public DateTime EnrollmentDate { get; set; }

        [Required]
        [DefaultValue(true)]
        public bool Activo { get; set; }

        public virtual ICollection<Enrollment> Enrollments { get; }
    }
}